<div class="span9">
    <legend>Selecione um período para gerar a lista de emails</legend>
    <?php echo form_open('painel/neswletters/seleciona'); ?>
    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label" for="data-de">De</label>
            <div class="controls">
              <?php echo form_textarea('data-de', set_value('data-de'), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('data-de'); ?></span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="data-para">Até</label>
            <div class="controls">
              <?php echo form_textarea('data-para', set_value('data-para'), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('data-para'); ?></span>
            </div>
        </div>
    </div>
</div><!--/span-->