<?php

Class Perfil extends DataMapper
{
    var $table = "user_profiles";
    var $has_one = array("user");
    var $has_many = array("logger");
    var $auto_populate_has_one = TRUE;
    
    function get_perfil($user_id)
    {
        
        
        $p = new Perfil();
        $p->where('user_id', $user_id);
        $p->limit(1);
        $p->get();
        
        return $p;
        
    }
    
    function check_perfil($user_id)
    {
        $p = new Perfil();
        $p->where('user_id', $user_id);
        $p->limit(1)->get();
        
        return $p->perfil_status;
    }
    
     function update_perfil($dados){
        
        $perfil = new Perfil();
        $perfil->where('user_id', $dados['id']);
        $update = $perfil->update(array(
            
                    'perfil_status' => $dados['perfil_status'],
                    'email' => $dados['email'],
                    'nome'  => $dados['nome'],
                    'nascimento'    => $dados['nascimento'],
                    'telefone'  => $dados['telefone'],
                    'celular'   =>  $dados['celular'],
                    'empresa'   =>  $dados['empresa'],
                    'rg'    =>  $dados['rg'],
                    'cpf_cnpj'  =>  $dados['cpf_cnpj'],
                    'endereco'  =>  $dados['endereco'],
                    'numero'    =>  $dados['numero'],
                    'complemento'   =>  $dados['id'],
                    'bairro'    =>  $dados['complemento'],
                    'cidade'    =>  $dados['cidade'],
                    'uf'    =>  $dados['uf'],
                    'cep'   =>  $dados['cep'],
                    'date_updated' => date('Y-m-d'),
                    'origem'  => $dados['origem'],
                    'origem_outros' => $dados['origem_outros']
            ));
        
        return $update;
       
    }
    
}

/* End of file file.php */
/* Location: ./application/moules/module/controllers/controller.php */