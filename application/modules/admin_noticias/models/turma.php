<?php

class Turma extends DataMapper {
    
    var $table = "turmas";
    var $has_one = array("item");
    var $has_many = array("aluno");
    
     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    
    function get_turmas($id)
    {
        $item = new Turma();
        $item->where('item_id', $id);
        $item->get();
        
        $arr = array();
        foreach($item->all as $turma)
        {
            $arr[] = $turma;
        }
        
        return $arr;
    }
    
    function get_turma($id)
    {
        $turma = new Turma();
        $turma->where('id', $id);
        $turma->limit(1);
        $turma->get();
        
        return $turma;
    }
    
    function get_item_id($id)
    {
        $turma = new Turma();
        $turma->where('id', $id);
        $turma->limit(1);
        $turma->get();
        
        return $turma->item_id;
    }
    
    function check_turma($id)
    {
        $turma = new Turma();
        $turma->where('id', $id);
        $turma->limit(1);
        $turma->get();
        
        if($turma->exists())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    
}

/* End of file employee.php */
/* Location: ./application/moules/atleta/models/atleta_model.php */