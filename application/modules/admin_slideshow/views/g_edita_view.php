<div class="span9">
<?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
<?php endif; ?>
<legend>Editar legenda</legend>
    <img src="<?php echo base_url(); ?>assets/img/fotos/<?php echo $foto->imagem; ?>"
    width="50%" height="50%" alt="<?php echo $foto->titulo; ?>">
    <?php echo form_open('adminfotos/atualiza'); ?>
      <?php echo form_hidden('id', $foto->id); ?>
      <?php echo form_hidden('tipo', $foto->categoria); ?>
      <div class="clearfix"></div>
      <br>
      <?php echo form_label('Legenda'); ?>
      <?php echo form_input('titulo', set_value('titulo', $foto->titulo)); ?>
      <div class="clearfix"></div>
      <input type="submit" name="test" value="Salvar" />
    </form>
</div>