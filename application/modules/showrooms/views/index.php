<div class="conteudo showroom">
    <div class="inner">
        <div class="clearfix"></div>
        <h1>Localização</h1>
        <div class="texto">
            <p>
                <?php echo $showroom->endereco ?> nº <?php echo $showroom->numero ?><br>
                <?php echo $showroom->bairro ?> - <?php echo $showroom->cidade ?> - <?php echo $showroom->uf ?> <br>
                <span class="icone-telefone"></span> pabx: <?php echo $showroom->tel ?><br>
                <span class="icone-email"></span> <?php echo $showroom->email ?>
            </p>
            <p class="horario"><span class="icone-relogio"></span> horario de funcionamento<br>
                segunda a sexta das <?php echo $showroom->inicio_seg_sex ?> às <?php echo $showroom->fim_seg_sex ?>.
            </p>
        </div>
        <div class="mapa">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.3242018923966!2d-46.59333528502235!3d-23.55679698468515!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5eb481732211%3A0x16583e0781abb995!2sR.+dos+Campineiros%2C+759+-+Mooca%2C+S%C3%A3o+Paulo+-+SP%2C+03167-020!5e0!3m2!1spt-BR!2sbr!4v1475258028138" width="425" height="350" frameborder="0" style="border:0" allowfullscreen>
        </div>
    </div>
</div>
