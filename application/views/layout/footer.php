            </div>
        <? if($pagina == 'home'): ?></div><? endif; ?>
        <footer <?= ($pagina == 'home' ? 'class="home"' : '') ?>>
            <div class="rodape-wrapper">
                <?php echo Modules::run('showrooms/parcial') ?>
            </div>
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
         <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script src="<?=base_url('assets/prettyphoto/js/jquery.prettyPhoto.js'); ?>"></script>

        <?php if ($pagina == 'home'): ?>
        <script>
            runSlider();
        </script>
        <?php endif; ?>
        <script id="produto_template" type="text/html">
            <a class="produto-lista-wrapper {{categoria}}" href="<?php echo site_url('produtos/{{categoria}}/detalhe/{{produto_id}}/{{offset}}') ?>">
                <div class="produto-thumb-wrapper">
                    <img src="<?php echo base_url('assets/img/produtos/lista/{{imagem}}') ?>" alt="{{titulo}}">
                </div>
                <div class="produto-texto-wrapper">
                    <h2 class="produto-titulo">{{titulo}}</h2>
                    <p class="produto-resumo">{{resumo}}</p>
                </div>
            </a>
        </script>

        <!--Start of Zopim Live Chat Script-->
        <script type="text/javascript">
        window.$zopim || (function(d, s) {
        var z = $zopim = function(c) {
        z._.push(c)
        }, $ = z.s = d.createElement(s), e = d.getElementsByTagName(s)[0];
        z.set = function(o) {
        z.set._.push(o)
        };
        z._ = [];
        z.set._ = [];
        $.async = !0;
        $.setAttribute("charset", "utf-8");
        $.src = "//v2.zopim.com/?38MW6r2X6a7gI7pz6ZSdMQG71lHKFcsB";
        z.t = +new Date;
        $.type = "text/javascript";
        e.parentNode.insertBefore($, e)
        })(document, "script");
        </script>
        <!--End of Zopim Live Chat Script-->

    </body>
</html>
