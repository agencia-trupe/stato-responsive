var gulp   = require('gulp'),
    sass   = require('gulp-sass'),
    concat = require('gulp-concat');

gulp.task('sass', function () {
  gulp.src('./assets/css/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(concat('main.css', {newLine: ''}))
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('default', ['sass'], function () {
  gulp.watch('./assets/css/*.scss', ['sass']);
});
